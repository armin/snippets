#!/usr/bin/env bash
# USAGE: $0 HOST:PORT
openssl s_client -connect "$1" < /dev/null 2>/dev/null | openssl x509 -pubkey -noout 2>/dev/null | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 | cut -d"=" -f 2- | xargs | tr '[:lower:]' '[:upper:]' | sed 's/\(..\)/\1:/g; s/.$//'



