/* Allow users in wheel group to run anything without authentication */
/* This file belongs in (e.g.): /etc/polkit-1/rules.d/10-nopasswd.rules */
polkit.addRule(function(action, subject) {
    if (subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
});




