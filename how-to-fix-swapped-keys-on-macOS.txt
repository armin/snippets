1) sudo plutil -convert xml1 /Library/Preferences/com.apple.keyboardtype.plist
2) edit file /Library/Preferences/com.apple.keyboardtype.plist and change type 43 to 41 (or the other way round, depending on what you have)
3) sudo plutil -convert binary1 /Library/Preferences/com.apple.keyboardtype.plist
Reboot. Now the < and the ^ keys should be swapped, if not, repeat steps 1-3 and reboot again.

