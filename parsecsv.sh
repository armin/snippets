#!/usr/bin/env bash

file="data.csv"
IFS=";"
columns=($(head -n1 "$file"))
tail -n +2 "$file" | while read -r line; do
  line=($line)
  for ((i=0;i<${#line};i++)); do
    echo "${columns[i]}: ${line[i]}"
  done
  echo
done

