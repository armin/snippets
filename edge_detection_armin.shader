uniform float sensitivity = 0.05;
uniform float multiplier = 1.0;
uniform float luminance = 0.3;
uniform float thickness = 1.0;
uniform bool invert_EDGE;
uniform float4 edge_color;
uniform bool edge_multiply;
uniform float4 non_edge_color;
uniform bool non_edge_multiply;
uniform bool alpha_channel;
uniform float alpha_level = 100;
uniform bool alpha_invert;
uniform float rand_f;

float4 mainImage(VertData v_in) : TARGET
{
	float4 color = image.Sample(textureSampler, v_in.uv);
	
	 float s = 3;
     float hstep = uv_pixel_interval.x * thickness;
     float vstep = uv_pixel_interval.y * thickness;
	
	float offsetx = (hstep * s) / 2.0;
	float offsety = (vstep * s) / 2.0;
	
	float4 lum = float4(luminance, luminance, luminance, 1 );
	float samples[9];
	
	int index = 0;
	for(int i = 0; i < s; i++){
		for(int j = 0; j < s; j++){
			samples[index] = dot(image.Sample(textureSampler, float2(v_in.uv.x + (i * hstep) - offsetx, v_in.uv.y + (j * vstep) - offsety )), lum);
			index++;
		}
	}
	
	float vert = samples[2] + samples[8] + (2 * samples[5]) - samples[0] - (2 * samples[3]) - samples[6];
	float hori = samples[6] + (2 * samples[7]) + samples[8] - samples[0] - (2 * samples[1]) - samples[2];
	float4 col;
	
	float o = ((vert * vert) + (hori * hori) * multiplier);
	bool isEdge = o > sensitivity;
	if(invert_EDGE){
		isEdge = !isEdge;
	}
	if(isEdge) {
		col = edge_color;
		if(edge_multiply){
			col *= color;
		}
	} else {
		col = non_edge_color;
		if(non_edge_multiply){
			col *= color;
		}
	}

	if (alpha_invert) {
		lum = 1.0 - lum;
	}

	if(alpha_channel){
		if (edge_multiply && isEdge) {
			return clamp(lerp(color, col, alpha_level), 0.0, 1.0);
		}
		else {
			return clamp(lerp(color, float4(max(color.r, col.r), max(color.g, col.g), max(color.b, col.b), 1.0), alpha_level), 0.0, 1.0);
		}
	} else {
		return col;
	}
}

