uniform float distortion<
    string label = "Distortion";
    string widget_type = "slider";
    float minimum = 5.0;
    float maximum = 1000.0;
    float step = 0.01;
> = 75.;

uniform float timedivisor<
    string label = "Time Divisor";
    string widget_type = "slider";
    float minimum = 0.0;
    float maximum = 100.0;
    float step = 0.01;
> = 1.;

uniform float amplitude<
    string label = "Amplitude";
    string widget_type = "slider";
    float minimum = 0.0;
    float maximum = 100.0;
    float step = 0.01;
> = 10.;

uniform float chroma<
    string label = "Chroma";
    string widget_type = "slider";
    float minimum = 0.0;
    float maximum = 6.28318531;
    float step = 0.01;
> = .5;

float2 zoomUv(float2 uv, float zoom) {
    float2 uv1 = uv;
    uv1 += .5;
    uv1 += zoom/2.-1.;
    uv1 /= zoom;
    return uv1;
}

float4 mainImage(VertData v_in) : TARGET {
    float2 uvt = v_in.uv;
    float2 uvtR = uvt;
    float2 uvtG = uvt;
    float2 uvtB = uvt;
    uvtR += float2(sin(-1. * (uvt.y*amplitude+elapsed_time/timedivisor*0.17))/distortion, cos(uvt.x*amplitude+elapsed_time/timedivisor)/distortion);
    uvtG += float2(cos(uvt.y*amplitude+elapsed_time/timedivisor+chroma)/distortion, cos(uvt.x*amplitude+elapsed_time/timedivisor+chroma)/distortion);
    uvtB += float2(sin(uvt.y*amplitude+elapsed_time/timedivisor*0.3+(chroma*2.))/distortion, cos(uvt.x*amplitude+elapsed_time/timedivisor+(chroma*2.))/distortion);
    float2 uvB = zoomUv(uvtR, 0.71 + (0.01*sin(elapsed_time)));
    float2 uvR = zoomUv(uvtG, 1.29 + (0.02*cos(elapsed_time)));
    float2 uvG = zoomUv(uvtB, 1.01 + (-0.01*sin(elapsed_time)));
    float4 colR = image.Sample(textureSampler, uvR);
    float4 colG = image.Sample(textureSampler, uvG);
    float4 colB = image.Sample(textureSampler, uvB);

    float blub = colR.r + colG.g + colB.b;

    if (blub > 0.03) { 
      return float4(colR.r, colG.g, colB.b, (colG.a + colB.a + colR.a) * elapsed_time);
    } else {
      return float4(0., 0., 0., 0.);
    };

}



