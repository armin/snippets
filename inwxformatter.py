#!/usr/bin/env python

# inwx formatter

# Reads a paste from INWX's domain search as a .txt file
# and shows the domains with prices

import re

def read_file_lines(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()
    return lines

def split_tld_price(line):
    result = re.split(r'(?<=[A-Za-z()我爱你商店娱乐みん游企业戏な])\s*(?=\d)', line)
    return result

filename = 'example.txt'  # Replace 'example.txt' with your file name
lines = read_file_lines(filename)
for i in lines:
    if "." in i:
        print("")
        print("Domain: " + i.rstrip())
    if i.rstrip().endswith('€') and "Verlängerung" not in i:
        r = split_tld_price(i.rstrip())
        print("Country ID: " + r[0])
        print("Initial: " + r[1].split('€')[0])
    if i.rstrip().endswith('€') and "Verlängerung" in i:
        print("Price: " + i.rstrip().split(':')[1].lstrip().split('€')[0].rstrip())

